
""" 
This script demonstrates the recursive reversal of a LinkedList

Example:
A -> B -> C -> D -> NULL
D -> C -> B -> A -> NULL

"""

class Node:

    def __init__(self, data=None):
        self.data = data 
        self.pointer = None 

    def __str__(self):
        return f"{self.data}"



class LinkedList:

    def __init__(self):
        self.head = None 
        self.tail = None 

    
    def append_node(self, node):
        if not isinstance(node, Node):
            node = Node(node)

        if not self.head:
            self.head = node
        else:
            self.tail.pointer = node
        self.tail = node


    def reverse_iteratively(self):

        current_node = self.head
        prev_node = None

        while current_node:
            next_node = current_node.pointer
            current_node.pointer = prev_node
            prev_node = current_node
            current_node = next_node

        self.head = prev_node 
            


    def reverse_recursively(self):
        
        def recursive_reversal(current_node, prev_node):
            if not current_node:
                return prev_node

            next_node = current_node.pointer
            current_node.pointer = prev_node
            prev_node = current_node 
            current_node = next_node
            return recursive_reversal(current_node, prev_node)

        self.head = recursive_reversal(current_node=self.head, prev_node=None)


    def __str__(self):
        current_node = self.head
        linked_list = ""
        
        while current_node:
            linked_list += str(current_node.data) + " -> "
            current_node = current_node.pointer

        if linked_list:
            return linked_list[:-4]
        else:
            return f"Linked List is Empty"



# Sample Linked List 
sample_list = LinkedList()

# Appending Nodes
sample_list.append_node("Python")
sample_list.append_node("C++")
sample_list.append_node("Javascript")

print(sample_list)

# Reverse Iteratively
sample_list.reverse_iteratively()
print(sample_list)

# Reverse Recursively
sample_list.reverse_recursively()
print(sample_list)